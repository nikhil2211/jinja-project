
from flask import Flask ,render_template
from datetime import datetime

app = Flask(__name__)

@app.route('/')
def randomimagegenerator():
    now = datetime.now()
    year = now.year
    month = now.month
    day = now.day
    time  = now.strftime("%H:%M:%S")
    monthlist = ['jan','feb','march','april','may','june','july','aug','sep','oct','nov','dec']
    return render_template('home.html',year = year,month = monthlist[month-1],day = day,time = time)
	


if __name__ == '__main__':

	app.run(debug=True)
